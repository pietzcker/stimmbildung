# README #

Dieses Skript erzeugt einen "hübschen" Stimmbildungsplan aus ComMusic-Daten

### Wie funktioniert's? ###

* Starten der Abfrage "Gesamtliste Stimmbildung (Vorlage für Übersichtsplan)
* Ziel der Abfrage: Zwischenablage
* Starten des Skripts Stimmbildungsplan-Generator.py

### Was brauche ich? ###

* Installation von ComMusic
* Import der Abfrage in den ComMusic-Reporter
* Installation von Python 3.7 oder höher
* Installation der Module xlsxwriter und win32clipboard
